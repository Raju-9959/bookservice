package com.bookservice.services;

import java.util.List;

import com.bookservice.jpa.Book;

public interface BookService {

	public List<Book> getAllBooks();

	public Book getBook(long id);

	public void deleteBook(long id);

	public Book addBook(Book book);

	public Book updateBook(long id, String desc, String name);

	public boolean validate(Book book);

}
