package com.bookservice.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookservice.exception.BookNotFoundException;
import com.bookservice.jpa.Book;
import com.bookservice.repository.BookRepository;

@Service
public class BookServiceImpl implements BookService {

	static Logger logger = LogManager.getLogger(BookServiceImpl.class);

	@Autowired
	private BookRepository bookrepo;

	@Override
	public List<Book> getAllBooks() {
		logger.info("getAllBooks Method Called");
		return bookrepo.findAll();
	}

	@Override
	public Book getBook(long id) {
		logger.info("getBook Method Called");
		return bookrepo.findById(id)
				.orElseThrow(() -> new BookNotFoundException("No Book Available With given Id" + id));
	}

	@Override
	public void deleteBook(long id) {
		logger.info("deleteBook Method Called");
		bookrepo.deleteById(id);
		logger.info("book is delete with id" + id);
	}

	@Override
	public Book addBook(Book book) {
		logger.info("addBook Method Called");
		return bookrepo.save(book);
	}

	@Override
	public Book updateBook(long id, String desc, String name) {
		logger.info("updateBook Method Called");
		Book book = bookrepo.findById(id)
				.orElseThrow(() -> new BookNotFoundException("No book found with given Id " + id));
		book.setName(name);
		book.setDescription(desc);
		return bookrepo.save(book);
	}

	@Override
	public boolean validate(Book book) {

		logger.info("validate Method Called");
		boolean status = true;
		if (book.getName().equals("") || book.getDescription().equals(""))
			status = false;
		logger.info("validateBook status is :{}", status);
		return status;
	}

}
