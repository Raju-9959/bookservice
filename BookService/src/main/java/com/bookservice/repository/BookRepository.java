package com.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookservice.jpa.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{

}
