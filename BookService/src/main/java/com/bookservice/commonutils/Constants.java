package com.bookservice.commonutils;

public enum Constants {
	SUCCESSFUL("Successful"), UNSUCCESSFUL("Unsuccessfull"), DELETED("Successfully Deleted"),
	UPDATED("Successfully Updated"), CREATED("Successfully Created"), NO_BOOKS("No Books Available"),
	NO_USERS("No Users Available"), INVALID_PARAMERTES("Please Check the all the parameters passed");

	String msg;

	private Constants(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return this.msg;
	}
}
