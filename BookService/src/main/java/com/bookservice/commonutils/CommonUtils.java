package com.bookservice.commonutils;

public class CommonUtils {

	public static boolean validate(String... params) {
		boolean status = true;
		for (String str : params) {
			if (str.equals("")) {
				status = false;
				break;
			}
		}
		return status;
	}

}
