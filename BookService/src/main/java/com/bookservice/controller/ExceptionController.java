package com.bookservice.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bookservice.exception.BookNotFoundException;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

	static Logger logger = LogManager.getLogger(ExceptionController.class);

	@ExceptionHandler(BookNotFoundException.class)
	public ResponseEntity<Response> bookExceptionHandler(BookNotFoundException ex) {
		logger.error("Exception in server with message :{}", ex.getMessage());
		return new ResponseEntity<>(new Response(ex.getMessage(), null), HttpStatus.NOT_FOUND);
	}


	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> exceptionHandler(Exception ex) {
		logger.error("Exception in server with message :{}", ex.getMessage());
		return new ResponseEntity<>(new Response(ex.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
