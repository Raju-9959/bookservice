package com.bookservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.bookservice.commonutils.CommonUtils;
import com.bookservice.commonutils.Constants;
import com.bookservice.jpa.Book;
import com.bookservice.services.BookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("books")
@Api(value = "Book Resource")
@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added"),
		@ApiResponse(code = 200, message = "The request has been succeeded "),
		@ApiResponse(code = 500, message = "Internal Server error"),
		@ApiResponse(code = 404, message = "Thre resource is not found"),
		@ApiResponse(code = 400, message = "The requested resource is not allowed"),
		@ApiResponse(code = 204, message = "No resources are not available")})
@CrossOrigin(origins = "*",allowedHeaders = "*")
public class BookController {

	private static final Logger logger = LogManager.getLogger(BookController.class);

	@Autowired
	private BookService bookServ;

	@Value("${spring.datasource.url}")
	private String value;

	@GetMapping("property")
	public String getValue() {
		return value;
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Getting all the books", response = ResponseEntity.class)
	public ResponseEntity<Response> get() {
		logger.info("/books is called with get method");
		ResponseEntity<Response> re = null;
		List<Book> books = bookServ.getAllBooks();
		if (books.isEmpty())
			re = new ResponseEntity<>(new Response(Constants.NO_BOOKS.getMsg(), null), HttpStatus.NO_CONTENT);
		else
			re = new ResponseEntity<>(new Response(Constants.SUCCESSFUL.getMsg(), books), HttpStatus.OK);
		logger.info("response status is {}", re.getStatusCode());
		return re;
	}

	@GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Getting a book", response = ResponseEntity.class)
	public ResponseEntity<Response> getBook(@PathVariable long id) {
		logger.info("/books/{id} is called with get methods");
		return new ResponseEntity<>(new Response(Constants.SUCCESSFUL.getMsg(), bookServ.getBook(id)), HttpStatus.OK);
		// if no book found throws no book found exception
		// and returns 404 from controllerAdvice
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Adding a book", response = ResponseEntity.class)
	public ResponseEntity<Response> addBook(@RequestBody Book book) {
		logger.info("/books/ is called with post method with request body");
		ResponseEntity<Response> re = null;
		if (bookServ.validate(book)) {
			re = new ResponseEntity<>(new Response(Constants.CREATED.getMsg(), bookServ.addBook(book)),
					HttpStatus.CREATED);
		} else {
			re = new ResponseEntity<>(new Response(Constants.INVALID_PARAMERTES.getMsg(), null),
					HttpStatus.BAD_REQUEST);
		}
		logger.info("response status is {}", re.getStatusCode());
		return re;
	}

	@DeleteMapping(value="{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Deleting a book", response = ResponseEntity.class)
	public ResponseEntity<Response> deleteBook(@PathVariable long id) {
		logger.info("/books/{id} is called with delete method");
		Book book = bookServ.getBook(id);// if no book found thorws NoBookFoundException
											// and returns 404 from controllerAdivce
		bookServ.deleteBook(id);

		return new ResponseEntity<>(new Response(Constants.DELETED.getMsg(), book), HttpStatus.OK);
	}

	@PutMapping(value="{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update a book", response = ResponseEntity.class)
	public ResponseEntity<Response> updateBook(@PathVariable long id, @RequestParam String desc,
			@RequestParam String name) {
		logger.info("/books/{id} is called with patch method and with two params desc and name");
		ResponseEntity<Response> re = null;
		if (CommonUtils.validate(desc, name)) {
			re = new ResponseEntity<>(new Response(Constants.UPDATED.getMsg(), bookServ.updateBook(id, desc, name)),
					HttpStatus.OK);
		} else
			re = new ResponseEntity<>(new Response(Constants.INVALID_PARAMERTES.getMsg(), null),
					HttpStatus.BAD_REQUEST);
		logger.info("response status is {}", re.getStatusCode());

		return re;
	}

}
