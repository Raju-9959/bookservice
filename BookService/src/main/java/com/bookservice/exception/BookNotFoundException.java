package com.bookservice.exception;


@SuppressWarnings("serial")
public class BookNotFoundException extends RuntimeException {

	public BookNotFoundException(String msg) {

		super(msg);
	}

}
