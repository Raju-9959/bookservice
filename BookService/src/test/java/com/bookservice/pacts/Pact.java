package com.bookservice.pacts;

import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import com.bookservice.BookServiceApplication;
import au.com.dius.pact.provider.junit.PactRunner;
import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;

@RunWith(PactRunner.class)
@Provider("book-service-app")
@PactFolder("/Users/Raju_Gangeru/Documents/Pact/Tests")
public class Pact {
	@TestTarget
	public final Target target = new HttpTarget("http", "localhost", 8090, "/");
	private static ConfigurableWebApplicationContext application;

	@BeforeClass
	public static void start() {
		application = (ConfigurableWebApplicationContext) SpringApplication.run(BookServiceApplication.class);
	}


	@State("Get Book")
	public void getBook(){
	}

	@State("Get Books")
	public void getBooks(){
	}

	@State("delete a  Book")
	public void deleteBook(){
	}

	@State("update a  Book")
	public void updateBook(){
	}

	@State("Add a  Book")
	public void AddBook()
	{
	}
}
