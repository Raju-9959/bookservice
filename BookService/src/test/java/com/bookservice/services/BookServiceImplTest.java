package com.bookservice.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.bookservice.exception.BookNotFoundException;
import com.bookservice.jpa.Book;
import com.bookservice.repository.BookRepository;

class BookServiceImplTest {

	@InjectMocks
	BookServiceImpl bookService;

	@Mock
	BookRepository bookRepo;

	static List<Book> books;
	static List<Book> newBooks;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeAll
	public static void setValues() {
		books = new ArrayList<>();
		newBooks = new ArrayList<>();
		books.add(new Book(1l, "testBook1", "testdesc1"));
		books.add(new Book(2l, "testBook2", "testdesc2"));
		newBooks.addAll(books);
		newBooks.add(new Book(3l, "testBook3", "testdesc3"));
	}

	@Test
	void testGetAllBooks() {
		Mockito.when(bookRepo.findAll()).thenReturn(books);
		Assertions.assertEquals(books, bookService.getAllBooks());
		Mockito.verify(bookRepo).findAll();
	}

	@Test
	void testGetAllBooksNegative() {
		Mockito.when(bookRepo.findAll()).thenReturn(books);
		Assertions.assertNotEquals(newBooks, bookService.getAllBooks());
		Mockito.verify(bookRepo).findAll();
	}

	@Test
	void testGetBook() {
		Mockito.when(bookRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(books.get(0)));
		Assertions.assertEquals(books.get(0), bookService.getBook(Mockito.anyLong()));
	}

	@Test
	void testGetBookNoBookFound() {
		Mockito.when(bookRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
		Assertions.assertThrows(BookNotFoundException.class, () -> bookService.getBook(Mockito.anyLong()));
	}

	@Test
	void testDeleteBook() {
		Mockito.doNothing().when(bookRepo).deleteById(Mockito.anyLong());
		bookService.deleteBook(Mockito.anyLong());
		Mockito.verify(bookRepo).deleteById(Mockito.anyLong());
	}

	@Test
	void testAddBook() {
		Mockito.when(bookRepo.save(books.get(0))).thenReturn(books.get(0));
		Assertions.assertEquals(books.get(0), bookService.addBook(books.get(0)));
	}

	@Test
	void testUpdateBook() {

		Mockito.when(bookRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(books.get(0)));
		Mockito.when(bookRepo.save(Mockito.any(Book.class))).thenReturn(books.get(0));
		Assertions.assertEquals(books.get(0), bookService.updateBook(Mockito.anyLong(), "test", "test"));

	}

	@Test
	void testValidate() {
		Assertions.assertEquals(true, bookService.validate(books.get(0)));
	}

	@Test
	void testValidateFalseName() {
		Book newBook = new Book(4l, "", "desc");
		Assertions.assertNotEquals(true, bookService.validate(newBook));
	}

	@Test
	void testValidateFalseDesc() {
		Book newBook = new Book(4l, "name", "");
		Assertions.assertNotEquals(true, bookService.validate(newBook));
	}

}
