package com.bookservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bookservice.jpa.Book;
import com.bookservice.services.BookService;

class BookControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	BookController bookController;

	@Mock
	BookService bookServ;

	static List<Book> books;
	static List<Book> newBooks;

	@BeforeAll
	public static void setValues() {
		books = new ArrayList<>();
		newBooks = new ArrayList<>();
		books.add(new Book(1l, "testBook1", "testdesc1"));
		books.add(new Book(2l, "testBook2", "testdesc2"));
		newBooks.addAll(books);
		newBooks.add(new Book(3l, "testBook3", "testdesc3"));
	}

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(bookController).build();

	}

	@Test
	void testGet() throws Exception {
		Mockito.when(bookServ.getAllBooks()).thenReturn(books);
		this.mockMvc.perform(get("/books")).andExpect(status().isOk());
	}

	@Test
	void testGetBody() throws Exception {
		Mockito.when(bookServ.getAllBooks()).thenReturn(books);
		Assertions.assertEquals(books, bookController.get().getBody().getBody());
	}

	@Test
	void testGetNoContent() throws Exception {
		Mockito.when(bookServ.getAllBooks()).thenReturn(new ArrayList<Book>());
		this.mockMvc.perform(get("/books")).andExpect(status().isNoContent());
	}

	@Test
	void testGetBook() throws Exception {
		Mockito.when(bookServ.getBook(Mockito.anyLong())).thenReturn(books.get(0));
		this.mockMvc.perform(get("/books/1")).andExpect(status().isOk());
	}
	
	@Test
	void testGetBookBody() throws Exception {
		Mockito.when(bookServ.getBook(Mockito.anyLong())).thenReturn(books.get(0));
		
		Assertions.assertEquals(books.get(0), bookController.getBook(Mockito.anyLong()).getBody().getBody());
	}

	@Test
	void testDeleteBook() throws Exception {
		Mockito.when(bookServ.getBook(Mockito.anyLong())).thenReturn(books.get(0));
		Mockito.doNothing().when(bookServ).deleteBook(Mockito.anyLong());
		this.mockMvc.perform(delete("/books/1")).andExpect(status().isOk());
	}

	/*
	 * @Test void testUpdateBook() throws Exception {
	 * Mockito.when(bookServ.updateBook(Mockito.anyLong(), Mockito.eq("testdesc"),
	 * Mockito.eq("testName"))) .thenReturn(books.get(0));
	 * this.mockMvc.perform(patch("/books/1").param("desc",
	 * "testDesc").param("name", "testName")) .andExpect(status().isOk()); }
	 */

	/*
	 * @Test void testUpdateBookBody() throws Exception {
	 * Mockito.when(bookServ.updateBook(Mockito.anyLong(), Mockito.eq("testdesc"),
	 * Mockito.eq("testName"))) .thenReturn(books.get(0));
	 * Assertions.assertEquals(books.get(0),
	 * bookController.updateBook(Mockito.anyLong(), Mockito.eq("testdesc"),
	 * Mockito.eq("testName")).getBody().getBody()); }
	 */

	/*
	 * @Test void testUpdateNoDesc() throws Exception {
	 * Mockito.when(bookServ.updateBook(Mockito.anyLong(), Mockito.eq(""),
	 * Mockito.eq("testName"))) .thenReturn(books.get(0));
	 * this.mockMvc.perform(patch("/books/1").param("desc", "").param("name",
	 * "testName")) .andExpect(status().isBadRequest()); }
	 */

	/*
	 * @Test void testUpdateNoName() throws Exception {
	 * Mockito.when(bookServ.updateBook(Mockito.anyLong(), Mockito.eq("testdesc"),
	 * Mockito.eq(""))) .thenReturn(books.get(0));
	 * this.mockMvc.perform(patch("/books/1").param("desc",
	 * "testDesc").param("name", "")) .andExpect(status().isBadRequest()); }
	 */

}
